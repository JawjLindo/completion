const spawn = require('child_process').spawn;
const Through = require('through2');
const path = require('path');

module.exports = function(options) {
  const paths = [];

  return Through.obj((file, encoding, callback) => {
    paths.push(file.path);
    callback();
  }, function (callback) {
    const stream = this;

    const labCommand = [path.join(process.cwd(), '/node_modules/@hapi/lab/bin/lab'),];
    const processArgs = labCommand.concat(paths);

    const childProcess = spawn(process.execPath, processArgs, {stdio: 'inherit',});

    childProcess.on('exit', (code) => {
      if (code !== 0
        && Object.prototype.toString.call(options) === '[object Object]'
        && options.failWithError) {
        stream.emit('error', 'Lab exited with errors.');
      } else {
        stream.emit('end');
      }

      callback();
    });
  });
};