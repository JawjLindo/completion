const gulp = require('gulp');
const lab = require('../plugins/gulp-lab-runner');

gulp.task('unit-tests', (done) => {
  gulp.src('test/unit/**/*.js')
    .pipe(lab({failWithError: false,}))
    .on('error', (err) => done(err))
    .on('end', done);
});

gulp.task('component-tests', (done) => {
  gulp.src('test/component/**/*.js')
    .pipe(lab({failWithError: false,}))
    .on('error', (err) => done(err))
    .on('end', done);
});

gulp.task('test', gulp.series('unit-tests', 'component-tests'));

// Watch files and execute the tests when modified
gulp.task('dev', gulp.series('unit-tests', 'component-tests', (done) => {
  gulp.watch([
    'src/**/*.js',
    'test/**/*.js',
  ], gulp.series('unit-tests', 'component-tests'))
  .on('end', done);
}));