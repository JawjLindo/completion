import Hapi from '@hapi/hapi';
import glob from 'glob';
import path from 'path';
import Blipp from 'blipp';

import { initializeDb, } from './utilities/data-utilities';

const server = Hapi.server({
  host: '0.0.0.0',
  port: '3000',
});

const start = async () => {
  try {
    await server.register(Blipp);
  
    // eslint-disable-next-line require-atomic-updates
    server.app.db = await initializeDb();
    // Load all the routes
    glob.sync(`${__dirname}/routes/*.js`)
      .forEach(file => require(path.resolve(file))(server));

    if (!global.isTesting) {    
      await server.start();
      console.info(`Server running on port: ${server.info.port}`);
    }
  } catch (err) {
    console.error(err);
    process.exit(1);
  }
};

start();

export default server;