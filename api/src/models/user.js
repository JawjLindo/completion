const collectionName = 'users';

export const UserErrors = Object.freeze({
  FirstNameMissing: Symbol('FirstNameMissing'),
  LastNameMissing: Symbol('LastNameMissing'),
  UserDoesNotExist: Symbol('UserDoesNotExist'),
  DatabaseWrite: Symbol('DatabaseWrite'),
});

export class User {
  /*
  Require that the model is always instantiated with the actual Mongo DB object passed in at runtime
  This allows for abstracting the model from the actual connection and client information
  */
  constructor(db) {
    if (!db) {
      throw new Error('A database object must be passed into the model');
    }

    this.collection = db.collection(collectionName);
  }

  async getById(userId) {
    return await this.collection.findOne({_id: userId,});
  }

  async update(userId, user) {
    // Check for valid input values and, for efficiency, return before calling the DB
    if (!user.firstName) return Promise.reject({message: 'The first name for the user is required', type: UserErrors.FirstNameMissing,});
    if (!user.lastName) return Promise.reject({message: 'The last name for the user is required', type: UserErrors.LastNameMissing,});

    const result = await this.collection.updateOne({_id: userId,}, user);
    if (result.matchedCount == 0) return Promise.reject({message: 'The user does not exist in the database and can, therefore, not be updated', type: UserErrors.UserDoesNotExist,});
    if (result.modifiedCount == 0) return Promise.reject({message: 'The database write failed', type: UserErrors.DatabaseWrite,});

    return Promise.resolve();
  }

  async delete(userId) {
    const result = await this.collection.deleteOne({_id: userId,});
    if (result.n == 0) return Promise.reject({message: 'The user does not exist in the database and can, therefore, not be deleted', type: UserErrors.UserDoesNotExist,});
  }

  async create(user) {
    // Check for valid input values and, for efficiency, return before calling the DB
    if (!user.firstName) return Promise.reject({message: 'The first name for the user is required', type: UserErrors.FirstNameMissing,});
    if (!user.lastName) return Promise.reject({message: 'The last name for the user is required', type: UserErrors.LastNameMissing,});

    const result = await this.collection.insertOne(user);
    if (result.insertedCount == 0) return Promise.reject({message: 'The database write failed', type: UserErrors.DatabaseWrite,});
  }
}

