import UserController from '../controllers/user-controller';

module.exports = (server) => {
  const controller = new UserController(server);

  server.route({
    method: 'GET',
    path: '/users/{id}',
    options: {
      handler: controller.getById,
    },
  });

  server.route({
    method: 'POST',
    path: '/users',
    options: {
      handler: controller.create,
    },
  });

  server.route({
    method: 'PUT',
    path: '/users/{id}',
    options: {
      handler: controller.replace,
    },
  });

  server.route({
    method: 'DELETE',
    path: '/users/{id}',
    options: {
      handler: controller.remove,
    },
  });

};