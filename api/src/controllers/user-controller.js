import bindProperties from '../utilities/property-binder';
import {User, UserErrors, } from '../models/user';
import Boom from '@hapi/boom';

export default class UserController {
  constructor(server) {
    this._user = new User(server.app.db);

    bindProperties(this, ['getById', 'replace', 'create', 'remove', ]);
  }

  async getById(request, h) {
    try {
      const userId = request.params.userId;
      const userFromStore = await this._user.getById(userId);

      const statusCode = userFromStore ? 200 : 404;

      return h.response(userFromStore)
        .type('application/json')
        .code(statusCode);
    } catch (err) {
      return h.response(Boom.badImplementation(err.message))
        .type('application/json');
    }
  }

  async replace(request, h) {
    try {
      const userId = request.params.userId;
      const replacementUser = request.payload;

      await this._user.update(userId, replacementUser);
      return h.response()
        .code(200);
    } catch (err) {
      if (err.type === UserErrors.FirstNameMissing || err.type === UserErrors.LastNameMissing) {
        throw Boom.badRequest(err.message);
      }

      if (err.type === UserErrors.UserDoesNotExist) {
        throw Boom.notFound(err.message);
      }
      
      if (err.type === UserErrors.DatabaseWrite) {
        throw Boom.badImplementation(err.message);
      }

      throw Boom.badImplementation(err.message);
    }
  }

  async create(request, h) {
    try {
      const newUser = request.payload;

      await this._user.create(newUser);

      return h.response()
        .code(201);
    } catch (err) {
      if (err.type === UserErrors.FirstNameMissing || err.type === UserErrors.LastNameMissing) {
        throw Boom.badRequest(err.message);
      }
      
      if (err.type === UserErrors.DatabaseWrite) {
        throw Boom.badImplementation(err.message);
      }

      throw Boom.badImplementation(err.message);
    }
  }

  async remove(request, h) {
    try {
      const userId = request.params.userId;

      await this._user.delete(userId);
      return h.response()
        .code(204);
    } catch (err) {
      if (err.type === UserErrors.UserDoesNotExist) {
        return h.response()
          .code(204);
      }

      throw Boom.badImplementation(err.message);
    }
  }
}