import MongoDB from 'mongodb';
const MongoClient = MongoDB.MongoClient;

export async function initializeDb() {
  if (!process.env.DB_HOST || !process.env.DB_PORT) {
    throw new Error('The database host or port environment variables have not been set.');
  }

  const client = await MongoClient.connect(`mongodb://${process.env.DB_HOST}:${process.env.DB_PORT}`);
  return await client.db('completion');
}