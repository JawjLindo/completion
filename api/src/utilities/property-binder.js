export default (context, props) => {
  props.forEach(prop => {
    context[prop] = context[prop].bind(context);
  });
};