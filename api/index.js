/*
This file is purely for running the server in a development environment without needing the transpile the code first.
This file is not to be used in production!
*/

require('@babel/polyfill');
require('@babel/register');
require('./src/index');