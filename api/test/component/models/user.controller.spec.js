// Since these tests are able to be run prior to transpilation, we must include the following two modules
require('@babel/polyfill');
require('@babel/register');

const lab = exports.lab = require('@hapi/lab').script();
const { expect, } = require('@hapi/code');

const sinon = require('sinon');
const MongoClient = require('mongodb').MongoClient;

// Set up the db and collection mock that will be passed into the model
const mockCollection = {
  findOne: null,
  updateOne: null,
  deleteOne: null,
  insertOne: null,
};
const mockDb = {
  collection: () => {
    return mockCollection;
  },
};
const mockDbClient = {
  db: async () => {
    return Promise.resolve(mockDb);
  },
};

lab.experiment('User component', () => {
  let server;
  const fakes = {
    MongoClient: null,
  };

  lab.before(() => {
    fakes.MongoClient = sinon.replace(MongoClient, 'connect', sinon.fake.resolves(mockDbClient));

    global.isTesting = true;
    server = require('../../../src/index').default;
    console.info(server.plugins.blipp.text());
  });

  lab.after(() => {
    sinon.restore();

    global.isTesting = null;
  });

  lab.afterEach(() => {
    // After each test, make sure the test-specific implementations are dropped
    mockCollection.findOne = null;
    mockCollection.updateOne = null;
    mockCollection.deleteOne = null;
    mockCollection.insertOne = null;
  });

  lab.experiment('retrieval', {skip: false,}, () => {
    lab.test('should successfully retrieve a User that exists in the database', async () => {
      const userId = '1234';

      const expectedFirstName = 'First';
      const expectedLastName = 'User';
      mockCollection.findOne = async () => {
        return {
          id: userId,
          firstName: expectedFirstName,
          lastName: expectedLastName,
        };
      };

      const serverOptions = {
        method: 'GET',
        url: `/users/${userId}`,
      };
      let result = await server.inject(serverOptions);

      expect(result.statusCode).to.equal(200);
      expect(result.result.id).to.equal(userId);
      expect(result.result.firstName).to.equal(expectedFirstName);
      expect(result.result.lastName).to.equal(expectedLastName);
    });

    lab.test('should not return a User that does not exist in the database', async () => {
      const userId = '99999';

      mockCollection.findOne = async () => {
        return null;
      };

      const serverOptions = {
        method: 'GET',
        url: `/users/${userId}`,
      };
      let result = await server.inject(serverOptions);

      expect(result.statusCode).to.equal(404);
    });
  });

  lab.experiment('creation', {skip: false,}, () => {
    lab.test('should successfully save a User when all values are valid', async () => {
      const newUser = {
        userId: '2345',
        firstName: 'John',
        lastName: 'Smith',
      };

      mockCollection.insertOne = () => {
        return {
          insertedCount: 1,
        };
      };

      const serverOptions = {
        method: 'POST',
        url: '/users',
        payload: newUser,
      };
      let result = await server.inject(serverOptions);

      expect(result.statusCode).to.equal(201);
    });

    lab.test('should fail to save a User that does not have a first name', async () => {
      const newUser = {
        userId: '2345',
        lastName: 'Smith',
      };

      const serverOptions = {
        method: 'POST',
        url: '/users',
        payload: newUser,
      };
      let result = await server.inject(serverOptions);

      expect(result.statusCode).to.equal(400);
      expect(result.result.message).to.equal('The first name for the user is required');
    });

    lab.test('should fail to save a User that does not have a last name', async () => {
      const newUser = {
        userId: '2345',
        firstName: 'John',
      };

      const serverOptions = {
        method: 'POST',
        url: '/users',
        payload: newUser,
      };
      let result = await server.inject(serverOptions);

      expect(result.statusCode).to.equal(400);
      expect(result.result.message).to.equal('The last name for the user is required');
    });
  });

  lab.experiment('updating', {only: false, skip: false,}, () => {
    lab.test('should successfully update a User when all values are valid and the User exists in the database', async () => {
      const userId = '9876';
      const updatedUser = {
        firstName: 'Jane',
        lastName: 'Doe',
      };

      mockCollection.updateOne = () => {
        return {
          modifiedCount: 1,
        };
      };

      const serverOptions = {
        method: 'PUT',
        url: `/users/${userId}`,
        payload: updatedUser,
      };
      let result = await server.inject(serverOptions);

      expect(result.statusCode).to.equal(200);
    });

    lab.test('should fail to update a User that does not have a first name', async () => {
      const userId = '9876';
      const updatedUser = {
        lastName: 'Doe',
      };

      const serverOptions = {
        method: 'PUT',
        url: `/users/${userId}`,
        payload: updatedUser,
      };
      let result = await server.inject(serverOptions);

      expect(result.statusCode).to.equal(400);
      expect(result.result.message).to.equal('The first name for the user is required');
    });

    lab.test('should fail to update a User that does not have a last name', async () => {
      const userId = '9876';
      const updatedUser = {
        firstName: 'Jane',
      };

      const serverOptions = {
        method: 'PUT',
        url: `/users/${userId}`,
        payload: updatedUser,
      };
      let result = await server.inject(serverOptions);

      expect(result.statusCode).to.equal(400);
      expect(result.result.message).to.equal('The last name for the user is required');
    });

    lab.test('should fail to update a User that does not exist in the database', async () => {
      const userId = '99999';
      const updatedUser = {
        firstName: 'Jane',
        lastName: 'Doe',
      };

      mockCollection.updateOne = () => {
        return {
          matchedCount: 0,
        };
      };

      const serverOptions = {
        method: 'PUT',
        url: `/users/${userId}`,
        payload: updatedUser,
      };
      let result = await server.inject(serverOptions);

      expect(result.statusCode).to.equal(404);
      expect(result.result.message).to.equal('The user does not exist in the database and can, therefore, not be updated');
    });
  });

  lab.experiment('deleting', {skip: false,}, () => {
    lab.test('should successfull delete a User that is in the database', async () => {
      const userId = '8765';

      mockCollection.deleteOne = async () => {
        return {
          n: 1,
        };
      };

      const serverOptions = {
        method: 'DELETE',
        url: `/users/${userId}`,
      };
      let result = await server.inject(serverOptions);

      expect(result.statusCode).to.equal(204);
    });

    lab.test('should fail to delete a User that is not in the database', async () => {
      const userId = '99999';

      mockCollection.deleteOne = async () => {
        return {
          n: 0,
        };
      };

      const serverOptions = {
        method: 'DELETE',
        url: `/users/${userId}`,
      };
      let result = await server.inject(serverOptions);

      expect(result.statusCode).to.equal(204);
    });
  });
});