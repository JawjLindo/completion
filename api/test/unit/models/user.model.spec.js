// Since these tests are able to be run prior to transpilation, we must include the following two modules
require('@babel/polyfill');
require('@babel/register');

const lab = exports.lab = require('@hapi/lab').script();
const { expect, } = require('@hapi/code');

const { User, UserErrors,} = require('../../../src/models/user');

// Set up the db and collection mock that will be passed into the model
const mockCollection = {
  findOne: null,
  updateOne: null,
  deleteOne: null,
  insertOne: null,
};
const mockDb = {
  collection: () => {
    return mockCollection;
  },
};

lab.experiment('User model', () => {
  let user;

  lab.before(() => {
    // Instantiate the model and pass the mock DB
    user = new User(mockDb);
  });

  lab.afterEach(() => {
    // After each test, make sure the test-specific implementations are dropped
    mockCollection.findOne = null;
    mockCollection.updateOne = null;
    mockCollection.deleteOne = null;
    mockCollection.insertOne = null;
  });

  lab.experiment('retrieval', {skip: false,}, () => {
    lab.test('should successfully retrieve a User that exists in the database', async () => {
      const userId = '1234';

      const expectedFirstName = 'First';
      const expectedLastName = 'User';

      mockCollection.findOne = async () => {
        return {
          id: userId,
          firstName: expectedFirstName,
          lastName: expectedLastName,
        };
      };
      const result = await user.getById(userId);

      expect(result.id).to.equal(userId);
      expect(result.firstName).to.equal(expectedFirstName);
      expect(result.lastName).to.equal(expectedLastName);
    });

    lab.test('should not return a User that does not exist in the database', async () => {
      const userId = '99999';

      mockCollection.findOne = async () => {
        return null;
      };
      const result = await user.getById(userId);

      expect(result).to.be.null();
    });
  });

  lab.experiment('creation', {skip: false,}, () => {
    lab.test('should successfully save a User when all values are valid', async () => {
      const newUser = {
        userId: '2345',
        firstName: 'John',
        lastName: 'Smith',
      };

      mockCollection.insertOne = () => {
        return {
          insertedCount: 1,
        };
      };
      await user.create(newUser);
    });

    lab.test('should fail to save a User that does not have a first name', async () => {
      const newUser = {
        userId: '2345',
        lastName: 'Smith',
      };

      try {
        await user.create(newUser);
      } catch (err) {
        expect(err.message).to.equal('The first name for the user is required');
        expect(err.type).to.equal(UserErrors.FirstNameMissing);
      }
    });

    lab.test('should fail to save a User that does not have a last name', async () => {
      const newUser = {
        userId: '2345',
        firstName: 'John',
      };

      try {
        await user.create(newUser);
      } catch (err) {
        expect(err.message).to.equal('The last name for the user is required');
        expect(err.type).to.equal(UserErrors.LastNameMissing);
      }
    });
  });

  lab.experiment('updating', {skip: false,}, () => {
    lab.test('should successfully update a User when all values are valid and the User exists in the database', async () => {
      const updatedUser = {
        userId: '9876',
        firstName: 'Jane',
        lastName: 'Doe',
      };

      mockCollection.updateOne = () => {
        return {
          modifiedCount: 1,
        };
      };
      await user.update(updatedUser.userId, updatedUser);
    });

    lab.test('should fail to update a User that does not have a first name', async () => {
      const updatedUser = {
        userId: '9876',
        lastName: 'Doe',
      };

      try {
        await user.update(updatedUser.userId, updatedUser);
      } catch (err) {
        expect(err.message).to.equal('The first name for the user is required');
        expect(err.type).to.equal(UserErrors.FirstNameMissing);
      }
    });

    lab.test('should fail to update a User that does not have a last name', async () => {
      const updatedUser = {
        userId: '9876',
        firstName: 'Jane',
      };

      try {
        await user.update(updatedUser.userId, updatedUser);
      } catch (err) {
        expect(err.message).to.equal('The last name for the user is required');
        expect(err.type).to.equal(UserErrors.LastNameMissing);
      }
    });

    lab.test('should fail to update a User that does not exist in the database', async () => {
      const updatedUser = {
        userId: '99999',
        firstName: 'Jane',
        lastName: 'Doe',
      };

      mockCollection.updateOne = () => {
        return {
          matchedCount: 0,
        };
      };

      try {
        await user.update(updatedUser.userId, updatedUser);
      } catch (err) {
        expect(err.message).to.equal('The user does not exist in the database and can, therefore, not be updated');
        expect(err.type).to.equal(UserErrors.UserDoesNotExist);
      }
    });
  });

  lab.experiment('deleting', {skip: false,}, () => {
    lab.test('should successfull delete a User that is in the database', async () => {
      const userId = '8765';

      mockCollection.deleteOne = async () => {
        return {
          n: 1,
        };
      };
      await user.delete(userId);
    });

    lab.test('should fail to delete a User that is not in the database', async () => {
      const userId = '99999';

      mockCollection.deleteOne = async () => {
        return {
          n: 0,
        };
      };

      try {
        await user.delete(userId);
      } catch (err) {
        expect(err.message).to.equal('The user does not exist in the database and can, therefore, not be deleted');
        expect(err.type).to.equal(UserErrors.UserDoesNotExist);
      }
    });
  });
});