const gulp = require('gulp');
const path = require('path');
const glob = require('glob');


// Load all the gulp tasks from the gulp_tasks folder
glob.sync('./gulp/tasks/**/*.js')
  .forEach(file => require(path.resolve(file)));

// The default task should not be used. Specific tasks are required.
gulp.task('default', (done) => {
  console.info(`Please run one of these tasks specifically by name: \n${gulp.tree().nodes}`);
  done();
});