module.exports = {
  coverage: false,
  lint: true,
  verbose: true,
  leaks: false,
};