# Complete Testing Example
The purpose of this project is to show the various levels of testing that can, and should, be performed.

- Unit Tests
- Component Tests
- Integration Tests
- System Tests
- Possible Others (TBD)
  - Stress Tests
  - Performance Tests

## Running the Docker containers
**API Layer - Unit Tests Only**
```sh
docker-compose run --rm --service-ports --no-deps api /bin/bash
```
**API Layer - Component Tests (required the DB)**
```sh
docker-compose run --rm --service-ports api /bin/bash
```